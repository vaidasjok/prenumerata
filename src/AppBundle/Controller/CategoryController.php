<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Utils\Subscription;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CategoryController extends Controller
{
    


    /**
     * @Route("/kategorijos/sarasas", name="categories_list")
     */
    public function createAction(Request $request, Subscription $subscription) 
    {
        $list = $subscription->read_categories_to_array();
        
        $this->container->get('session')->remove('errors');
        
        $form = $this->createFormBuilder()
            ->add('kategorija', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $new_category = $form->getData();
             $subscription->write_category($new_category['kategorija']);

             $this->addFlash(
                'notice',
                'Jūs sėkmingai pridėjote kategoriją!'
            );
            
            return $this->redirectToRoute('categories_list');
        }

        return $this->render('default/categories/list.html.twig', array(
            'list' => $list,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/kategorijos/sarasas/trinti/{category_to_delete}", name="categories_delete")
     */
    public function deleteAction(Request $request, Subscription $subscription, $category_to_delete) 
    {
        $subscription->delete_category($category_to_delete);
        return $this->redirectToRoute('categories_list');
    }


}
