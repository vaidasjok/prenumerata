<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Utils\Subscription;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Session\Session;


class SubscriptionController extends Controller
{
    

    /**
     * @Route("/forma/prenumerata/sarasas", name="subscription_list")
     */
    public function indexAction(Subscription $subscription, Session $session) 
    {
        $list = $subscription->read_subscription_to_assoc_array();
        $errors = $this->container->get('session')->get('errors');
        $this->container->get('session')->remove('errors');
        if($session->get('admin') == 'true') {
            return $this->render('default/forma/list.html.twig', array(
                'list' => $list,
                'errors' => $errors
            ));
        } else {
            return $this->redirectToRoute('login');
        }
        
        
    }


    /**
     * @Route("/forma", name="forma")
     */
    public function createAction(Request $request, Subscription $subscription)
    {
        $kategorijos_pries_paruosiant_formai = $subscription->read_categories_to_array();
        $kategorijos = [];

        foreach($kategorijos_pries_paruosiant_formai as $kategorija) {
            $kategorijos[trim($kategorija)] = trim($kategorija);
        }

        $form = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('kategorija', ChoiceType::class, array(
                'placeholder' => 'Pasirinkite kategoriją',
                'multiple' => true,
                'expanded' => false,
                'choices'  => $kategorijos,
                ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $new_subscription = $form->getData();
             $subscription->write_subscription($new_subscription['kategorija'], $new_subscription['name'], $new_subscription['email']);

             $this->addFlash(
                'notice',
                'Jūs sėkmingai pasirinkote naujienas!'
            );
            
            return $this->redirectToRoute('forma_thanks');
        }
        return $this->render('default/forma/index.html.twig', array(
                'form' => $form->createView(),
                'kategorijos' => $kategorijos
            ));

    }


    /**
     * @Route("/forma/thanks", name="forma_thanks")
     */
    public function thanksAction() 
    {

        return $this->render('default/forma/thanks.html.twig', array(
            
        ));
    }
    

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, Session $session) 
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('password', TextType::class)
            ->getForm();
        


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $new_login = $form->getData();
             // var_dump($new_login['name']);
             // die();
             if($new_login["name"] == "admin" && $new_login["password"] == "password") {
                $session->set('admin', 'true');
                $this->addFlash(
                    'notice',
                    'Jūs sėkmingai prisiregistravote.'
                );
                return $this->redirectToRoute('subscription_list');
             }
            $this->addFlash(
                'notice',
                'Neteisingi prisijungimo duomenys.'
            );
             
            
            
        }
        return $this->render('default/forma/login.html.twig', array(
                'form' => $form->createView()
            ));

    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Session $session) {
        $session->remove('admin');
        return $this->redirectToRoute('login');
    }



    /**
     * @Route("/forma/prenumerata/sarasas/rikiuoti/{by}", name="subscription_sort")
     */
    public function SortAction(Subscription $subscription, $by) 
    {
        if($by == 'date') {
            $subscription->sort_subscription_by_date();
        } elseif($by == 'name') {
            $subscription->sort_subscription_by_name();
        } elseif($by == 'email') {
            $subscription->sort_subscription_by_email();
        }

        return $this->redirectToRoute('subscription_list');
    }


    /**
     * @Route("/forma/prenumerata/sarasas/redaguoti", name="subscription_edit")
     */
    public function editAction(Request $request, Subscription $subscription, ValidatorInterface $validator) 
    {
        // validation prasideda
        $email = $request->request->get('email');
        $new_email = $request->request->get('new_email');
        $name = $request->request->get('name');
        $errors = array();


        $emailConstraint = new Assert\Email();
        $emailConstraint->message = 'Netinkamas adresas, prašome pataisyti.';
        $errorList = $validator->validate($new_email, $emailConstraint);
        if(!empty($errorList[0])) {
            array_push($errors, $errorList[0]->getMessage());
        }
        

        $emailEmptyConstraint = new Assert\NotBlank();
        $emailEmptyConstraint->message = 'Adreso laukelis negali būti tuščias, prašome pataisyti.';
        $errorList = $validator->validate($new_email, $emailEmptyConstraint);
        if(!empty($errorList[0])) {
            array_push($errors, $errorList[0]->getMessage());
        }


        $namelEmptyConstraint = new Assert\NotBlank();
        $namelEmptyConstraint->message = 'Vardo laukelis negali būti tuščias, prašome pataisyti.';
        $errorList = $validator->validate($name, $namelEmptyConstraint);
        if(!empty($errorList[0])) {
            array_push($errors, $errorList[0]->getMessage());
        }
        // validation ends
        if (0 == count($errors)) {
            $subscription->change_email_and_name_in_subscribtion($email, $name, $new_email);
            return $this->redirectToRoute('subscription_list');
        } else {
            if(count($errors) > 0) {
                $this->container->get('session')->set('errors', $errors);
            }
            // var_dump($errorList);
            // die();
            return $this->redirectToRoute('subscription_list');
        }
        
    }
    

    /**
     * @Route("/forma/prenumerata/sarasas/trinti/{email}", name="subscription_delete")
     */
    public function deleteAction(Subscription $subscription, $email) 
    {
        $delete = $subscription->delete_from_subscription($email);
        $list = $subscription->read_subscription_to_assoc_array();
        return $this->redirectToRoute('subscription_list');
    }

}
