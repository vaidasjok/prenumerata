<?php 

namespace AppBundle\Utils;

class Subscription 
{

	// Keliai į failus, atstojančius duomenų bazes.
	private $subscriptions;
	private $kategorijos;

	public function __construct() { 
		$this->subscriptions = dirname(dirname( __FILE__ )) . 
			DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . 'subscriptions.txt';
		$this->kategorijos = dirname(dirname( __FILE__ )) . DIRECTORY_SEPARATOR . 
			'Files' . DIRECTORY_SEPARATOR . 'kategorijos.txt';
	}

	//Naujos kategorijos įrašymas į 'kategorijos.txt'.
	public function write_category($category) 

	{
		if($handle = fopen($this->kategorijos, 'a')) {
			if($category != "") {
				fwrite($handle, $category . "\n");
				fclose($handle);
			}
			
			
		} else {
			
			throw new Exception("Negalima buvo atidaryti failo rašymui.");
		}
	}

	// Kategorijų sąrašas perkeliamas į masyvą, kad būtų galima pateikti dizaino 
	// šablone.
	public function read_categories_to_array() 
	{
		if($handle = fopen($this->kategorijos, 'r')) {
			$categories_array = array();
			while(!feof($handle)) {
				array_push($categories_array, fgets($handle));
			}
			fclose($handle);
			
		} else {
			throw new Exception("Negalima buvo atidaryti failo kategorijų skaitymui.");
		}
		return $categories_array;
	}

	// Kategorijų trynimas.
	public function delete_category($category_to_delete) 
	{	
		$categories = $this->read_categories_to_array();
		foreach($categories as $key => $category) {
			if($category == $category_to_delete ) {
				unset($categories[$key]);
			}
		}
		//trinamas categories failo turinys
		if($handle = fopen($this->kategorijos, 'w')) {
			file_put_contents($this->kategorijos, "");
		}
		fclose($handle);
		
		// keiciamas subscribtion failo turinys
		if($handle = fopen($this->kategorijos, 'a')) {
			file_put_contents($this->kategorijos, "");
			foreach($categories as $category) {
				
				$content = $category;
				fwrite($handle, $content);
			}
			
			fclose($handle);
			
		} else {
			throw new Exception($exception_message);
		}
	}

	//Naujo prenumerato įrašo darymas.
	public function write_subscription(array $categories, $name, $email) 
	{
		
		if($handle = fopen($this->subscriptions, 'a')) {
			$categories = implode($categories, ", ");
			$timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
			$content = $timestamp . " | " . $name . " | " . $email . " | " . 
				$categories;
			fwrite($handle, $content . "\n");
			fclose($handle);
			
		} else {
			throw new Exception("Negalima buvo atidaryti failo prenumeratos rašymui.");
		}
	}

	// Pagalbinė funkcija, kuri paverčia prenumeratos sąrašą masyvu. Masyvas 
	// naudojamas prenumeratos sąrašo duomenų manipuliavimui.
	public function read_subscription_to_assoc_array() 
	{
		
		if($handle = fopen($this->subscriptions, 'r')) {
			$temporary_array = array();
			$content = array();
			$content_array = array();
			while(!feof($handle)) {
				$temporary_array = explode(" | ", fgets($handle));
				if(count($temporary_array) > 1) {
					$content['date'] = $temporary_array[0];
					$content['name'] = $temporary_array[1];
					$content['email'] = $temporary_array[2];
					$content['categories'] = $temporary_array[3];
					array_push($content_array, $content);
				}
				
			}
			fclose($handle);
			return $content_array;
		} else {
			throw new Exception("Negalima buvo atidaryti failo prenumeratos skaitymui.");
		}
	}

	// Pagalbine funkcija, kurios tikslas - perrašyti prenumeratos sąrašą 
	// (subscribtions.txt) masyvo pagalba.
	private function rewrite_subscribtion_using_array(
			array $array, 
			$exception_message
		) 
	{
		// keiciamas subscribtion failo turinys
		if($handle = fopen($this->subscriptions, 'a')) {
			file_put_contents($this->subscriptions, "");
			foreach($array as $element) {
				if(count($element) > 1) {
					$content = $element['date']; 
					$content .= " | ";
					$content .= $element['name'];
					$content .= " | ";
					$content .= $element['email'];
					$content .= " | ";
					$content .= $element['categories'];
				fwrite($handle, $content);
				}
				
			}
			
			fclose($handle);
			
		} else {
			throw new Exception($exception_message);
		}

	}

	//Prenumeratos įrašo trynimas pagal elektroninio ašto adresą. El-pašto adresas naudojamas vietoje id.
	public function delete_from_subscription($email) 
	{
		$working_array = $this->read_subscription_to_assoc_array();
		$keys = array();
		foreach($working_array as $key => $sub_array) {
		    if($sub_array['email'] == $email) {
		        unset($working_array[$key]);
		        // break; 
		    }
		}	
		//trinamas subscription failo turinys
		if($handle = fopen($this->subscriptions, 'w')) {
			file_put_contents($this->subscriptions, "");
		}
		fclose($handle);
		// keiciamas subscribtion failo turinys
		$this->rewrite_subscribtion_using_array($working_array, "Nepavyko ištrinti prenumeratos įrašo.");
	}

	// Prenumeratos sąrašo rūšiavimas pagal prenumeratos vartotojo vardą. Žemiau esanti statinis 
	// metodas dirba kartu su po juo sekančiu metodu.
	static function custom_sort_by_name($a,$b) 
	{
		return $a['name']>$b['name'];
	}

	public function sort_subscription_by_name() 
	{
		$array_to_sort = $this->read_subscription_to_assoc_array();
		usort($array_to_sort, array($this, "custom_sort_by_name"));
		//trinamas subscription failo turinys
		if($handle = fopen($this->subscriptions, 'w')) {
			file_put_contents($this->subscriptions, "");
		}
		fclose($handle);
		// keiciamas subscribtion failo turinys
		$this->rewrite_subscribtion_using_array($array_to_sort, "Nepavyko surūšiuoti prenumeratos sąrašo 
			pagal vartotojo vardą.");
	}

	// Prenumeratos sąrašo rūšiavimas pagal email. Žemiau esanti statinis metodas dirba kartu su sekančiu metodu.
	static function custom_sort_by_email($a,$b) 
	{
		return $a['email']>$b['email'];
	}

	public function sort_subscription_by_email() 
	{
		$array_to_sort = $this->read_subscription_to_assoc_array();
		usort($array_to_sort, array($this, "custom_sort_by_email"));
		//trinamas subscription failo turinys
		if($handle = fopen($this->subscriptions, 'w')) {
			file_put_contents($this->subscriptions, "");
		}
		fclose($handle);
		// keiciamas subscribtion failo turinys
		$this->rewrite_subscribtion_using_array($array_to_sort, "Nepavyko surūšiuoti prenumeratos sąrašo 
			pagal elektroninio pašto adresą.");
	}
    
	//Prenumeratos sąrašo rūšiavimas pagal datą. Žemiau esanti statinis metodas dirba kartu su sekančiu metodu.
    static function custom_sort_by_date($a,$b) 
    {
		return $a['date']>$b['date'];
	}

	public function sort_subscription_by_date() 
	{
		$array_to_sort = $this->read_subscription_to_assoc_array();
		usort($array_to_sort, array($this, "custom_sort_by_date"));
		//trinamas subscription failo turinys
		if($handle = fopen($this->subscriptions, 'w')) {
			file_put_contents($this->subscriptions, "");
		}
		fclose($handle);
		// keiciamas subscribtion failo turinys
		$this->rewrite_subscribtion_using_array($array_to_sort, "Nepavyko surūšiuoti prenumeratos sąrašo pagal datą.");
	}
	
	//Vartotojo vardo keitimas prenumeratos (subscribtions.txt) sąraše.
	// $email sąraše atstoja id vaidmenį, nepaisant to, kad jis gali kartotis kelis kartus.
	public function change_name_in_subscribtion($email, $name) 
	{
		$subscribtion_array = $this->read_subscription_to_assoc_array();
		foreach($subscribtion_array as &$subscribtion) {
			if($subscribtion['email'] == $email) {
				$subscribtion['name'] = $name;
			}
		}

		$this->rewrite_subscribtion_using_array($subscribtion_array, "Nepavyko pakeisti prenumeratos vartotojo vardo.");
	}

	// Vartotojo vardo keitimas prenumeratos (subscribtions.txt) sąraše.
	// $email sąraše atstoja id vaidmenį, nepaisant to, kad jis gali kartotis kelis kartus.
	public function change_email_in_subscribtion($email, $new_email) 
	{
		$subscribtion_array = $this->read_subscription_to_assoc_array();
		foreach($subscribtion_array as &$subscribtion) {
			if($subscribtion['email'] == $email) {
				$subscribtion['email'] = $new_email;
			}
		}
		$this->rewrite_subscribtion_using_array($subscribtion_array, "Nepavyko pakeisti prenumeratos vartotojo vardo.");
	}

	// Vartotojo vardo ir elektroninio pašto adreso keitimas prenumeratos (subscribtions.txt) sąraše.
	// $email sąraše atstoja id vaidmenį, nepaisant to, kad jis gali kartotis kelis kartus.
	// Šis metodas apjungia dviejų aukščiau esančių metodų funkcionalųmą.
	public function change_email_and_name_in_subscribtion($email, $name = null, $new_email = null) 
	{
		$subscribtion_array = $this->read_subscription_to_assoc_array();
		foreach($subscribtion_array as &$subscribtion) {
			if($subscribtion['email'] == $email) {
				if(isset($name)) {
					$subscribtion['name'] = $name;
				}
				if(isset($new_email)) {
					$subscribtion['email'] = $new_email;
				}
			}
		}
		$this->rewrite_subscribtion_using_array($subscribtion_array, "Nepavyko pakeisti prenumeratos vartotojo vardo.");
	}

}